// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PlayerAnimatorManager.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Networking Demos
// </copyright>
// <summary>
//  Used in PUN Basics Tutorial to deal with the networked player Animator Component controls.
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using Photon.Pun;

public class PlayerAnimatorManager : MonoBehaviourPun
{
	#region Private Fields

	[SerializeField]
	private float directionDampTime = 0.25f;
	Animator animator;

	float turnX;

	float turnY;

	#endregion

	#region MonoBehaviour CallBacks

	/// <summary>
	/// MonoBehaviour method called on GameObject by Unity during initialization phase.
	/// </summary>
	void Start()
	{
		animator = GetComponent<Animator>();

	}

	/// <summary>
	/// MonoBehaviour method called on GameObject by Unity on every frame.
	/// </summary>
	void Update()
	{
		


			// Prevent control is connected to Photon and represent the localPlayer
			if (photonView.IsMine)
			{
				Debug.LogError(GetComponent<Rigidbody>().velocity.magnitude);

				if (GetComponent<Rigidbody>().velocity.magnitude > 0)
				{
					GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
				}

			}


		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}

		// failSafe is missing Animator component on GameObject
		if (!animator)
		{
			return;
		}

		// deal with Jumping
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

		// only allow jumping if we are running.
		if (stateInfo.IsName("Base Layer.Run"))
		{
			// When using trigger parameter
			if (Input.GetButtonDown("Fire2")) animator.SetTrigger("Jump");
		}

		// deal with movement
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		Debug.Log("h" + h);
		Debug.Log("v" + v);

		turnX += Input.GetAxis("Mouse X");

		turnY += Input.GetAxis("Mouse Y");

		transform.localRotation = Quaternion.Euler(-turnY, turnX, 0);

		GetComponent<Rigidbody>().AddForce(this.transform.forward * v * 100f);
		GetComponent<Rigidbody>().AddForce(this.transform.right * h * 100f);

		if (GetComponent<Rigidbody>().velocity.magnitude > 0)
		{
			GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
		}

		// set the Animator Parameters
		animator.SetFloat("Speed", h * h + v * v);
		//animator.SetFloat( "Direction", h, directionDampTime, Time.deltaTime );
	}

	#endregion

}