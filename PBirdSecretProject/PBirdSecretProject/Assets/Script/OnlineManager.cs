using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class OnlineManager : MonoBehaviour
{
    private static OnlineManager _instance;

    public static OnlineManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<OnlineManager>();
            }

            return _instance;
        }
    }
    public GameObject DynamicInstantiate(GameObject gameObj, Vector3 spawnPosition, Quaternion rotation)
    {
        if (PhotonNetwork.IsConnected)
        {
            return PhotonNetwork.Instantiate(gameObj.name, spawnPosition, rotation, 0);
        }
        else
        {
            return Instantiate(gameObj, spawnPosition, rotation);
        }
    }

    public void LoadLevel(string sceneName)
    {
        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.LoadLevel(sceneName);
            }
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
    }

    public bool IsConnected()
    {
        return PhotonNetwork.IsConnected;
    }
}
