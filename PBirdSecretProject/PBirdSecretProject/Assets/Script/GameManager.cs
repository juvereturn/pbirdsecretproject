using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;
using TMPro;

public class GameManager : MonoBehaviourPunCallbacks
{
    private static GameManager _instance;

    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    public bool theGameHasStarted = false;


    public bool gameOver = false;

    [SerializeField]
    private PhotonView myPV;

    [SerializeField]
    private GameObject speaker;

    [SerializeField]
    private GameObject robot;

    [SerializeField]
    private Text timeText;

    [SerializeField]
    private Text waitText;

    [SerializeField]
    private Text gameOverText;

    [SerializeField]
    private Text objectiveText;

    [SerializeField]
    private List<Transform> spawnPositions = new List<Transform>();

    private List<PlayerCharacter> runners = new List<PlayerCharacter>();

    public float time = 90f;

    private string currentPassword;

    [SerializeField]
    private List<string> passwords = new List<string> { "alice", "jojo", "bird", "bas", "juve" };

    [SerializeField]
    private List<TextMeshPro> codes = new List<TextMeshPro>();

    [SerializeField]
    private List<GameObject> exit = new List<GameObject>();

    [SerializeField]
    private TMP_InputField inputPasswordPanel;

    private void Start()
    {
        if (PhotonNetwork.IsConnected)
        {
            if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
            {
                OnlineManager.Instance.DynamicInstantiate(speaker, new Vector3(0, 0, 0), Quaternion.identity);

                waitText.text = "Press Left Kick To Start";

                objectiveText.text = "Objective: Help Runners Survive The Game";

                objectiveText.color = new Color(0, 1, 0);

                RandomCode();
            }
            else
            {
                GameObject playerCharacter = OnlineManager.Instance.DynamicInstantiate(robot, RandomSpawnPos(), Quaternion.identity);
                Cursor.lockState = CursorLockMode.Locked;
               /* if (PhotonNetwork.IsMasterClient) 
                {*/
                    playerCharacter.GetComponent<PlayerCharacter>().ActivePlayerName();
                //}

                Debug.LogError(PhotonNetwork.NickName);

                if (PhotonNetwork.LocalPlayer.ActorNumber >= 3)
                {
                    runners.Add(playerCharacter.GetComponent<PlayerCharacter>());

                    objectiveText.text = "Objective: Survive Within Time Limit Or Escape";

                    objectiveText.color = new Color(0, 1, 0);
                }
                else
                {
                    objectiveText.text = "Objective: Kill All Runners";

                    objectiveText.color = new Color(1, 0, 0);
                }
            }
        }
    }

    void UpdatePlayers() 
    {
        PlayerCharacter[] players = FindObjectsOfType<PlayerCharacter>();
        runners = new List<PlayerCharacter>();

        for (int i = 0; i < players.Length; i++) 
        {
            if (players[i].playerType == PlayerType.Runner) 
            {
                runners.Add(players[i]);
            }

                
        }
        

    }

    bool alreadyleave = false;

    private void Update()
    {
        if (theGameHasStarted)
        {
            if (gameOver == true)
            {
                if (!alreadyleave) 
                {
                    StartCoroutine(LeaveGame());
                    alreadyleave = true;
                }
                
            }

        }

        if (PhotonNetwork.IsMasterClient)
        {
            if (theGameHasStarted)
            {
                if (CheckAllPlayersDead())
                {
                    gameOver = true;

                    gameOverText.gameObject.SetActive(true);

                    myPV.RPC("RPC_SyncGameText", RpcTarget.OthersBuffered, "Chaser Win");

                    myPV.RPC("RPC_SyncGameOver", RpcTarget.OthersBuffered, gameOver);

                    return;
                }


                time -= Time.deltaTime;

                myPV.RPC("RPC_SyncTime", RpcTarget.OthersBuffered, time);

                if (time <= 0)
                {
                    time = 0;

                    gameOver = true;

                    gameOverText.gameObject.SetActive(true);

                    myPV.RPC("RPC_SyncGameText", RpcTarget.OthersBuffered, "Runners Win");

                    myPV.RPC("RPC_SyncGameOver", RpcTarget.OthersBuffered, gameOver);
                }

            }
            else
            {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        theGameHasStarted = true;

                        waitText.gameObject.SetActive(false);

                        inputPasswordPanel.gameObject.SetActive(true);

                        UpdatePlayers();

                        myPV.RPC("RPC_SyncStartGame", RpcTarget.OthersBuffered, theGameHasStarted);
                    }

                



            }

        }




        timeText.text = "Time: " + time.ToString() + "s";

    }

    public void EndGame() 
    {
        gameOver = true;

        gameOverText.gameObject.SetActive(true);

        myPV.RPC("RPC_SyncGameText", RpcTarget.OthersBuffered, "Runners Win");

        myPV.RPC("RPC_SyncGameOver", RpcTarget.OthersBuffered, gameOver);

        StartCoroutine(LeaveGame());
    }

    IEnumerator LeaveGame() 
    {
        yield return new WaitForSeconds(3f);
        Cursor.lockState = CursorLockMode.None;
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel(0);


    }

    void RandomCode() 
    {
        int randomcode = Random.Range(0, passwords.Count);

        currentPassword = passwords[randomcode];

        foreach (char c in currentPassword) 
        {
            int random = Random.Range(0, codes.Count - 1);

            codes[random].text = c.ToString();

            codes[random].gameObject.SetActive(true);

            myPV.RPC("RPC_SyncCodeText", RpcTarget.OthersBuffered, random, c.ToString());

            codes.RemoveAt(random);
        }
    }

    bool CheckAllPlayersDead()
    {
        if (runners.Count == 0) 
        {
            return false;
        }

        for (int i = 0; i < runners.Count; i++)
        {
            Debug.LogError("runners[].dead" + i + runners[i].dead);

            if (runners[i].dead == false)
            {
                return false;
            }
        }
        return true;
    }

    public Vector3 RandomSpawnPos()
    {
        if (spawnPositions == null || spawnPositions.Count <= 0)
        {
            return new Vector3(0, 0, 0);
        }
        else
        {
            int randomRange = Random.Range(0, spawnPositions.Count - 1);

            Vector3 spawnPos = spawnPositions[0].position;

            spawnPositions.RemoveAt(randomRange);

            return spawnPos;
        }
    }

    bool hasInputCorrectPassword = false;

    public void CheckCorrectPassword(string pass) 
    {
        if (pass == currentPassword) 
        {
            if (!hasInputCorrectPassword) 
            {
                int random = Random.Range(0, exit.Count - 1);

                exit[random].SetActive(false);
                myPV.RPC("RPC_SyncExitDoor", RpcTarget.OthersBuffered, random);
                hasInputCorrectPassword = true;
            }
        }
    }




    #region IPunObservable implementation

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this.time);
        }
        else
        {
            // Network player, receive data
            this.time = (float)stream.ReceiveNext();
        }
    }

    [PunRPC]
    public void RPC_SyncTime(float networkTime)
    {
        time = networkTime;
    }

    [PunRPC]
    public void RPC_SyncStartGame(bool startGame)
    {
        theGameHasStarted = startGame;

        if (theGameHasStarted)
        {
            waitText.gameObject.SetActive(false);
        }
    }
    [PunRPC]
    public void RPC_SyncGameOver(bool gameOverrr)
    {
        gameOver = gameOverrr;

        if (gameOverrr)
        {
            gameOverText.gameObject.SetActive(true);
        }
    }
    [PunRPC]
    public void RPC_SyncGameText(string gameOverrr)
    {
        gameOverText.text = gameOverrr;
    }

    [PunRPC]
    public void RPC_SyncCodeText(int index,string c)
    {
        codes[index].text = c.ToString();

        codes[index].gameObject.SetActive(true);
    }

    [PunRPC]
    public void RPC_SyncExitDoor(int index)
    {
        exit[index].SetActive(false);
    }
    #endregion
}