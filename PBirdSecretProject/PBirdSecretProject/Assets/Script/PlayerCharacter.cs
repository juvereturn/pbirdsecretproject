using UnityEngine.EventSystems;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
using TMPro;

public enum PlayerType
{
    Chaser,
    Runner
}

public class PlayerCharacter : MonoBehaviourPunCallbacks, IPunObservable
{
    #region Public Fields

    [Tooltip("The current Health of our player")]
    public float Health = 1f;

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    #endregion

    #region Private Fields

    [Tooltip("The Beams GameObject to control")]
    [SerializeField]
    private Beam beam;

    [SerializeField]
    private GameObject helpMe;

    [SerializeField]
    private PhotonView myPV;

    //True, when the user is firing
    bool IsFiring;

    public bool dead = false;

    public PlayerType playerType;

    #endregion

    [PunRPC]
    public void RPC_SyncPlayerType(PlayerType assignPlayerType)
    {
        if (photonView.IsMine) 
        {
            playerType = assignPlayerType;
        }
    }

    #region MonoBehaviour CallBacks

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during early initialization phase.
    /// </summary>
    public void Awake()
    {
        if (this.beam != null)
        {
            this.beam.SetBeamActive(false);
            if (PhotonNetwork.IsConnected)
            {
                if (photonView.IsMine) 
                {
                    if (PhotonNetwork.LocalPlayer.ActorNumber == 1)
                    {
                        //You are the watcher
                    }
                    else if (PhotonNetwork.LocalPlayer.ActorNumber == 2)
                    {
                        this.beam.AssignBeamType(BeamType.AttackingBeam);
                        playerType = PlayerType.Chaser;

                    }
                    else
                    {
                        this.beam.AssignBeamType(BeamType.HealingBeam);
                        playerType = PlayerType.Runner;

                    }
                    myPV.RPC("RPC_SyncCharacterType", RpcTarget.OthersBuffered, playerType);
                }
            }



        }

        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instanciation when levels are synchronized
        if (photonView.IsMine)
        {
            LocalPlayerInstance = gameObject;
        }
    }

    /// <summary>
    /// MonoBehaviour method called on GameObject by Unity during initialization phase.
    /// </summary>
    public void Start()
    {
        CameraWork _cameraWork = gameObject.GetComponent<CameraWork>();

        if (_cameraWork != null)
        {
            if (photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><b>Missing</b></Color> CameraWork Component on player Prefab.", this);
        }
    }

    public void Update()
    {
        // we only process Inputs and check health if we are the local player
        if (photonView.IsMine)
        {

            helpMe.SetActive(dead);



            Mathf.Clamp01(this.Health);

            if (this.Health <= 0f)
            {
                //dead
                dead = true;
            }
            else 
            {
                if (this.Health >= 1f) 
                {
                    this.Health = 1f;
                    dead = false;
                }

                this.ProcessInputs();
            }
        }

        myPV.RPC("RPC_SyncDead", RpcTarget.OthersBuffered, dead);

        if (this.beam != null && this.IsFiring != this.beam.gameObject.activeInHierarchy)
        {
            this.beam.SetBeamActive(this.IsFiring);
        }
    }

    /// <summary>
    /// MonoBehaviour method called when the Collider 'other' enters the trigger.
    /// Affect Health of the Player if the collider is a beam
    /// Note: when jumping and firing at the same, you'll find that the player's own beam intersects with itself
    /// One could move the collider further away to prevent this or check if the beam belongs to the player.
    /// </summary>
    public void OnTriggerEnter(Collider other)
    {
        if (!photonView.IsMine)
        {
            return;
        }


        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (other.GetComponent<Beam>() is Beam beam)
        {
            if (beam.BeamType == BeamType.AttackingBeam)
            {
                this.Health -= 0.1f;
            }
            else 
            {
                this.Health += 0.1f;
            }
            
        }

        if (other.GetComponent<WinCollision>())
        {
            if (playerType == PlayerType.Runner)
            {
                Debug.LogError("YOu win");
                GameManager.Instance.EndGame();
            }
            
        }

        
    }

    /// <summary>
    /// MonoBehaviour method called once per frame for every Collider 'other' that is touching the trigger.
    /// We're going to affect health while the beams are interesting the player
    /// </summary>
    /// <param name="other">Other.</param>
    public void OnTriggerStay(Collider other)
    {
        // we dont' do anything if we are not the local player.
        if (!photonView.IsMine)
        {
            return;
        }

        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (other.GetComponent<Beam>() is Beam beam)
        {
            if (beam.BeamType == BeamType.AttackingBeam)
            {
                Debug.LogError("Beam ATK");

                this.Health = -1f;
                Debug.LogError("Health" + Health);
            }
            else
            {
                Debug.LogError("Beam Heal");
                this.Health = 1f;
            }

        }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Processes the inputs. This MUST ONLY BE USED when the player has authority over this Networked GameObject (photonView.isMine == true)
    /// </summary>
    void ProcessInputs()
    {
        if (!GameManager.Instance.theGameHasStarted) 
        {
            return;
        }

        if (Input.GetButtonDown("Fire1"))
        {
            // we don't want to fire when we interact with UI buttons for example. IsPointerOverGameObject really means IsPointerOver*UI*GameObject
            // notice we don't use on on GetbuttonUp() few lines down, because one can mouse down, move over a UI element and release, which would lead to not lower the isFiring Flag.
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //	return;
            }

            if (!this.IsFiring)
            {
                this.IsFiring = true;
            }
        }

        if (Input.GetButtonUp("Fire1"))
        {
            if (this.IsFiring)
            {
                this.IsFiring = false;
            }
        }
    }

    #endregion

    [SerializeField]
    private TextMesh textName;

    public void ActivePlayerName() 
    {
        myPV.RPC("RPC_SyncPlayerName", RpcTarget.MasterClient);

    }

    [PunRPC]
    public void RPC_SyncPlayerName()
    {
        textName.gameObject.SetActive(true);
        textName.text = PhotonNetwork.NickName;
    }

    

    #region IPunObservable implementation

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(this.IsFiring);
            stream.SendNext(this.Health);
            stream.SendNext(this.dead);
        }
        else
        {
            // Network player, receive data
            this.IsFiring = (bool)stream.ReceiveNext();
            this.Health = (float)stream.ReceiveNext();
            this.dead = (bool)stream.ReceiveNext();
        }
    }


    [PunRPC]
    public void RPC_SyncDead(bool dead)
    {
        this.dead = dead;
        helpMe.SetActive(dead);
    }

    [PunRPC]
    public void RPC_SyncCharacterType(PlayerType type)
    {
        playerType = type;
    }


    #endregion
}
