using UnityEngine;
using Photon.Pun;
using Photon.Pun.Demo.PunBasics;
public enum BeamType 
{
    AttackingBeam,
    HealingBeam
}
public class Beam : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private PhotonView myPV;

    private BeamType beamType;

    public MeshRenderer BeamMesh;

    public MeshRenderer BeamMeshLeft;

    public MeshRenderer meshPlate;

    public Material redMaterial;

    public Material greenMaterial;

    public BeamType BeamType => beamType;

    public void AssignBeamType(BeamType assignBeamType)
    {
        if (photonView.IsMine) 
        {
            beamType = assignBeamType;

            if (beamType == BeamType.AttackingBeam)
            {
                Debug.LogError("Assign Red Material");
                BeamMesh.material = redMaterial;
                BeamMeshLeft.material = redMaterial;
                meshPlate.material = redMaterial;
            }
            else
            {
                Debug.LogError("Assign Green Material");
                BeamMesh.material = greenMaterial;
                BeamMeshLeft.material = greenMaterial;
                meshPlate.material = greenMaterial;
            }

            myPV.RPC("RPC_SyncBeamColor", RpcTarget.OthersBuffered, beamType);
        }


    }

    public void SetBeamActive(bool active) 
    {
        BeamMeshLeft.gameObject.SetActive(active);
        this.gameObject.SetActive(active);
    }

    [PunRPC]
    public void RPC_SyncBeamColor(BeamType assignBeamType)
    {
        beamType = assignBeamType;

        if (assignBeamType == BeamType.AttackingBeam)
        {
            //Debug.LogError("Assign Red Material");
            BeamMesh.material = redMaterial;
            BeamMeshLeft.material = redMaterial;
            meshPlate.material = redMaterial;
        }
        else
        {
            //Debug.LogError("Assign Green Material");
            BeamMesh.material = greenMaterial;
            BeamMeshLeft.material = greenMaterial;
            meshPlate.material = greenMaterial;
        }
    }
}

