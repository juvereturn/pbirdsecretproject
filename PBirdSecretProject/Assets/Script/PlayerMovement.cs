using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMovement : MonoBehaviourPun
{
	#region Private Fields

	[SerializeField]
	private float directionDampTime = 0.25f;
	Animator animator;

	float turnX;

	float turnY;

	PlayerCharacter playerCharacter;

	#endregion

	#region MonoBehaviour CallBacks

	/// <summary>
	/// MonoBehaviour method called on GameObject by Unity during initialization phase.
	/// </summary>
	void Start()
	{
		animator = GetComponent<Animator>();
		playerCharacter = GetComponent<PlayerCharacter>();
	}

	/// <summary>
	/// MonoBehaviour method called on GameObject by Unity on every frame.
	/// </summary>
	void Update()
	{
		if (!GameManager.Instance.theGameHasStarted) 
		{
			return;
		}

		

		// Prevent control is connected to Photon and represent the localPlayer
		if (photonView.IsMine)
		{


			if (GetComponent<Rigidbody>().velocity.magnitude > 0)
			{
				GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
			}
			if (GetComponent<Rigidbody>().angularVelocity.magnitude > 0)
			{
				GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
			}

			if (playerCharacter != null && playerCharacter.dead) 
			{
				return;
			}

		}


		if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
		{
			return;
		}

		// failSafe is missing Animator component on GameObject
		if (!animator)
		{
			return;
		}

		// deal with Jumping
		AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

		// only allow jumping if we are running.
		if (stateInfo.IsName("Base Layer.Run"))
		{
			// When using trigger parameter
			if (Input.GetButtonDown("Fire2")) animator.SetTrigger("Jump");
		}

		// deal with movement
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");

		Debug.Log("h" + h);
		Debug.Log("v" + v);

		turnX += Input.GetAxis("Mouse X");

		turnY += Input.GetAxis("Mouse Y");

		transform.localRotation = Quaternion.Euler(-turnY, turnX, 0);

		GetComponent<Rigidbody>().AddForce(this.transform.forward * v * 200f);
		GetComponent<Rigidbody>().AddForce(this.transform.right * h * 200f);

		if (GetComponent<Rigidbody>().velocity.magnitude > 0)
		{
			GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
		}

		// set the Animator Parameters
		animator.SetFloat("Speed", h * h + v * v);
		//animator.SetFloat( "Direction", h, directionDampTime, Time.deltaTime );
	}

	#endregion
}
